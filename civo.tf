resource "civo_kubernetes_cluster" "primary" {
    region = var.region
    name = var.name
    firewall_id = civo_firewall.primary.id
    pools {
        size = var.target_nodes_size
        node_count = var.num_target_nodes
    }
    timeouts{
        create = "10m"
    }
}
# Create a firewall
resource "civo_firewall" "primary" {
    name = var.name
    region = var.region
}



